app.config(function ($routeProvider, $locationProvider) {
    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
      });

    $routeProvider
        .when('/', {
            redirectTo: function () {
                return '/login';
            }
        })
        .when('/login', {
            templateUrl: '../../../login.html',
            controller: 'LoginController'
        })
        .when('/inicio', {
            templateUrl: '../../../inicio.html',
            controller: 'QRCodeController'
        })
        .when('/materia', {
            templateUrl: '../../../materia.html',
            controller: 'MateriaController'
        })
        .when('/bloco', {
            templateUrl: '../../../bloco.html',
            controller: 'BlocoController'
        })
        .when('/sala', {
            templateUrl: '../../../sala.html',
            controller: 'SalaController'
        })
        .when('/cadastro', {
            templateUrl: '../../../cadastro.html',
            controller: 'CadastroController'
        })
        .when('/perfil', {
            templateUrl: '../../../perfil.html',
            controller: 'PerfilController'
        })
        .otherwise({ redirectTo: '/inicio'})
})
