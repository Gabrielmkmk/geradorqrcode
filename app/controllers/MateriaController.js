(function () {
    app.controller("MateriaController", function ($timeout, $scope) {

        $scope.materiaParaDelecao = false;
        $scope.materiaDeletada = false;
        $scope.edicaoBlocoSalaMenu = true;

        $scope.diasDaSemana = ["Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado"];
        $scope.turno = ["Manhã", "Tarde", "Noite"];
        $scope.numeroAula = ["Aula 01", "Aula 02"];

        function initial() {
            getBlocos()
        }

        function getBlocos() {
            $scope.blocos = [];
            var blocos = firebase.database().ref('Bloco/');
            blocos.on('value', (snapshot) => {
                const data = snapshot.val();
                    $timeout(function() {
                        for(var item in data) {
                            $scope.blocos.push(item);
                        }
                    });
            });
        }

        function getSalas(bloco) {
            $scope.salas = [];
            var salas = firebase.database().ref('Bloco/' + bloco);
            salas.on('value', (snapshot) => {
                const data = snapshot.val();
                $timeout(function() {
                    for(var item in data){
                        if(item != "a")
                            $scope.salas.push(item);
                    }
                });
            });
        }

        $scope.editarBloco = function(blocoSelecionado) {
            $scope.edicaoNovoBloco = true;
            $scope.blocoParaEdicao = blocoSelecionado;
            editarBlocos(blocoSelecionado);
        }

        function editarBlocos(blocoSelecionado) {
            getSalas(blocoSelecionado);
        }

        $scope.cancelarEdicao = function() {
            $scope.edicaoNovoBloco = false;
            $scope.salas = [];
            $scope.blocoParaEdicao = undefined
            $scope.diaDaSemanaSelecionadoInt = undefined;
            $scope.turnoSelecionadoInt = undefined
            $scope.numeroAulaSelecionadoInt = undefined
            $scope.nomeMateria = "";
            $scope.salaEdit = false;
            getBlocos()
        }

        $scope.editarSala = function(salaEdicao) {
            $scope.edicaoBlocoSalaMenu = false;
            $scope.salaEdit = true;
            $scope.salaParaEdicao = salaEdicao;
        }

        $scope.buscarMateria = function(diaDaSemanaSelecionado, turnoSelecionado, numeroAulaSelecionado) {
            $scope.botaoRemoveAtivo = false;
            document.getElementById("nomeMateriaInput").value = "";
            $scope.diaDaSemanaSelecionadoInt;
            $scope.turnoSelecionadoInt;
            $scope.numeroAulaSelecionadoInt;
            for(var item in $scope.diasDaSemana) {
                if($scope.diasDaSemana[item] == diaDaSemanaSelecionado) {
                    $scope.diaDaSemanaSelecionadoInt = parseInt(item,10) + 2;
                }
            }
            for(var item2 in $scope.turno) {
                if($scope.turno[item2] == turnoSelecionado) {
                    $scope.turnoSelecionadoInt = parseInt(item2,10) + 1;
                }
            }
            for(var item3 in $scope.numeroAula) {
                if($scope.numeroAula[item3] == numeroAulaSelecionado) {
                    $scope.numeroAulaSelecionadoInt = parseInt(item3,10) + 1;
                }
            }
            var materia = firebase.database().ref('Bloco/' + $scope.blocoParaEdicao + "/" + $scope.salaParaEdicao + "/" + $scope.diaDaSemanaSelecionadoInt + "/" + $scope.turnoSelecionadoInt + "/" + $scope.numeroAulaSelecionadoInt + "/nomeMateria");
            materia.on('value', (snapshot) => {
                const data = snapshot.val();
                $timeout(function() {
                    $scope.materiaEncontrada = true;
                    if(data != null && data != "") {
                        $scope.nomeMateria = data.toString();
                        document.getElementById("nomeMateriaInput").value = data.toString();
                    } else {
                        if(!$scope.materiaParaDelecao) {
                            $scope.nomeMateria = "";
                            $scope.materiaNaoEncontrada = true;
                            $scope.mensagem = "A sala " + $scope.salaParaEdicao + " não possui aula cadastrada para o período informado."
                            $scope.botaoRemoveAtivo = true;
                            setTimeout(() => {
                                $timeout(function() {
                                    $scope.materiaNaoEncontrada = false;
                                    $scope.mensagem = ""
                                });
                            }, 3000)
                        }
                    }
                });
            });
        }

        $scope.adicionarMateria = function() {        
            const materiaNova = {
                nomeMateria: document.getElementById("nomeMateriaInput").value
            }
            if(materiaNova.nomeMateria == undefined || materiaNova.nomeMateria == null || materiaNova.nomeMateria == "") {
                $scope.naoAdicionar = true;
                $scope.mensagemErroAdicionar = "Preencha o nome da matéria!";
    
                setTimeout(() => {
                    $timeout(function() {
                        $scope.naoAdicionar = false;
                        $scope.mensagemErroAdicionar = "";
                    });
                }, 3000)
                return;
            }
            firebase.database().ref('Bloco/' + $scope.blocoParaEdicao + "/" + $scope.salaParaEdicao + "/" + $scope.diaDaSemanaSelecionadoInt + "/" + $scope.turnoSelecionadoInt + "/" + $scope.numeroAulaSelecionadoInt).set(materiaNova);
            $timeout(function() {
                $scope.materiaEditada = true;
                $scope.mensagemSucesso = "Matéria adicionada com sucesso!";
    
                setTimeout(() => {
                    $timeout(function() {
                        $scope.materiaEditada = false;
                        $scope.mensagemSucesso = "";
                    });
                }, 3000)
                document.getElementById("nomeMateriaInput").value = "";
                getSalas($scope.blocoParaEdicao)
            });
        }

        $scope.disableRemover = function() {
            $scope.botaoRemoveAtivo = true;
        }

        $scope.removerMateria = function() {
            $scope.materiaDeletada = true;
            $scope.materiaParaDelecao = true;
            $scope.mensagemDelecao = "Matéria deletada com sucesso!";
            $scope.salas = []
            var deleteMateria = firebase.database().ref('Bloco/' + $scope.blocoParaEdicao + "/" + $scope.salaParaEdicao + "/" + $scope.diaDaSemanaSelecionadoInt + "/" + $scope.turnoSelecionadoInt + "/" + $scope.numeroAulaSelecionadoInt);
            deleteMateria.remove()
            .then(function() {
                $timeout(function() {
                    $scope.materiaParaDelecao = false;
                    setTimeout(() => {
                        $timeout(function() {
                            $scope.materiaDeletada = false;
                            $scope.mensagemDelecao = "";
                        });
                    }, 3000)
                });

                document.getElementById("nomeMateriaInput").value = "";
                getSalas($scope.blocoParaEdicao)
                $scope.botaoRemoveAtivo = true;
            })
        }

        $scope.voltarParaBlocos = function() {
            $scope.edicaoBlocoSalaMenu = true;
            $scope.salaEdit = false;
            $scope.diaDaSemanaSelecionadoInt = undefined;
            $scope.turnoSelecionadoInt = undefined
            $scope.numeroAulaSelecionadoInt = undefined
            $scope.botaoRemoveAtivo = false;
            $scope.materiaEncontrada = false;
            document.getElementById("nomeMateriaInput").value = "";
            $scope.materiaNaoEncontrada = false;
            $scope.materiaDeletada = false;
            $scope.materiaEditada = false;
        }

        initial();
    });
}).call(angular);
