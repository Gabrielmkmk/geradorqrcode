(function () {
    app.controller("PerfilController", function ($timeout, $scope, $location, $rootScope) {

        function initial() {
            var dados = firebase.database().ref('Professor/' + $rootScope.usuarioLogadoId);
            dados.on('value', (snapshot) => {
                const data = snapshot.val();
                    $scope.nome = data.nome;
                    $scope.sobrenome = data.sobrenome;
                    $scope.codigo = data.codigo;
            });
        }

        $scope.editarProfessor = function(nome, sobrenome) {
            $rootScope.editandoProfessor = true;
            $scope.editando = true;
            const novoProfessor = {
                nome: nome,
                sobrenome: sobrenome,
                codigo: document.getElementById("codigo").value
            }
            firebase.database().ref('Professor/' + $rootScope.usuarioLogadoId).update(novoProfessor);
            $timeout(function() {
                $scope.editando = false;
                $scope.professorEditado = true;
                $scope.mensagem = "Professor editado com sucesso";
            })
            setTimeout(() => {
                $timeout(function() {
                    $scope.professorEditado = false;
                    $rootScope.editandoProfessor = false;
                    $scope.mensagem = ""
                });
            }, 3000)
        }

        $scope.deslogar = function() {
            $rootScope.editandoProfessor = undefined
            $rootScope.usuarioLogadoId = undefined
            $rootScope.usuarioLogado = undefined
            $rootScope.usuarioAdmin = undefined
            $location.path("/login")
        }

        initial();
    });
}).call(angular);
