(function () {
    app.controller("LoginController", function ($timeout, $scope, $location, $rootScope) {
        function initial() {
            $scope.falhaLogin = false;
            $scope.logando = false;
        }

        $scope.logar = function() {
            $scope.logando = true;
            var login = document.getElementById("login").value;
            var senha = document.getElementById("senha").value;
            if(login == '' || senha == '') {
                $scope.falhaLogin = true;
                $scope.logando = false;
                $scope.mensagem = "Por favor preencha todos os campos!";
                return;
            }
            firebase.auth().signInWithEmailAndPassword(login, senha).then((result) => {
                $timeout(function() {
                    var professor = firebase.database().ref('Professor/' + result.user.$.W);
                    professor.on('value', (snapshot) => {
                        if(snapshot.val() != null) {
                            $timeout(function() {
                                if(result.user.$.W == "CNSRftpKoEaXUsLvKhDk8EgS3IK2") {
                                    $timeout(function() {
                                        $rootScope.usuarioAdmin = true;
                                    })
                                }
                                $rootScope.usuarioLogadoId = result.user.$.W;
                                $rootScope.usuarioLogado = true;
                                if($rootScope.editandoProfessor == false || $rootScope.editandoProfessor == undefined) {
                                    $location.path("/inicio")
                                }
                            })
                        } else {
                            $timeout(function() {
                                $scope.falhaLogin = true;
                                $scope.logando = false;
                                $scope.mensagem = "Login ou Senha inválidos.";
                                setTimeout(() => {
                                    $timeout(function() {
                                        $scope.falhaLogin = false;
                                        $scope.mensagem = ""
                                    });
                                }, 3000)
                            });  
                        }
                    });
                });

            }).catch(function (error) {
                    $timeout(function() {
                        $scope.falhaLogin = true;
                        $scope.logando = false;
                        $scope.mensagem = "Login ou Senha inválidos.";
                        setTimeout(() => {
                            $timeout(function() {
                                $scope.falhaLogin = false;
                                $scope.mensagem = ""
                            });
                        }, 3000)
                    });  
            });
        };
        initial();
    });
}).call(angular);
