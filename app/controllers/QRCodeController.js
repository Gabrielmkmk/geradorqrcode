(function () {
    app.controller("QRCodeController", function ($timeout, $rootScope, $scope, $location) {

        $scope.blocos = [];
        $scope.salas = [];

        function initial() {
            getBlocos();
            if($rootScope.usuarioLogado == null) {
                $location.path("/login")
            }
        }

        $scope.trocarSalas = function(bloco) {
            $scope.blocoSelecionado = bloco;
            getSalas(bloco);
        }

        $scope.gerarQRCode = function() {
            var dataAtual = new Date();
            var diaDaSemana = dataAtual.getDay() + 1;
            debugger;
            var turno = getTurnoAtual(dataAtual);
            var numeroAula = getNumeroAula(dataAtual, turno);
            var nomeMateria = firebase.database().ref("Bloco/" + $scope.blocoSelecionado + "/" + $scope.salaSelecionada + "/" + diaDaSemana + "/" + turno + "/" + numeroAula);
            nomeMateria.on('value', (snapshot) => {
                const data = snapshot.val();
                if(data == null || data.nomeMateria == "") {
                    $scope.erroQRCode = true;
                    $scope.mensagemErro = "Não há aulas cadastradas para este período";
        
                    setTimeout(() => {
                        $timeout(function() {
                            $scope.erroQRCode = false;
                            $scope.mensagemErro = "";
                        });
                    }, 3000)
                } else {
                    document.getElementById('qrcode').innerHTML = "";
                    new QRCode(document.getElementById('qrcode'), data.nomeMateria);
                }
            });   
        }

        $scope.gerarPlanilha = function() {
            var dataAtual = new Date();
            var diaDaSemana = dataAtual.getDay() + 1;
            var turno = getTurnoAtual(dataAtual);
            var numeroAula = getNumeroAula(dataAtual, turno);
            var nomeMateria = firebase.database().ref("Bloco/" + $scope.blocoSelecionado + "/" + $scope.salaSelecionada + "/" + diaDaSemana + "/" + turno + "/" + numeroAula);
            nomeMateria.on('value', (snapshot) => {
                const data = snapshot.val();
                var dataPresenca = new Date();
                var meses = dataPresenca.getMonth() + 1;
                var dataFormatada = (("0" + dataPresenca.getDate()).slice(-2)) + "-" + (("0" + meses).slice(-2)) + "-" + dataPresenca.getFullYear();
                var presencas = [];
                var presenca = firebase.database().ref();
                var stringPresenca = "Presenca/"+data.nomeMateria+"-"+dataFormatada
                presenca.child(stringPresenca).get().then((snapshotPresenca) => {
                    snapshotPresenca.forEach(function(childNodes) {
                        var p = {
                            codigoAluno: childNodes.val().codigoAluno,
                            horarioEntrada: ("0" + childNodes.val().dataEntrada.hours).slice(-2) + ":" + ("0" + childNodes.val().dataEntrada.minutes).slice(-2)
                        };
                        if(childNodes.val().dataSaida) {
                            p.horarioSaida = ("0" + childNodes.val().dataSaida.hours).slice(-2) + ":" + ("0" + childNodes.val().dataSaida.minutes).slice(-2);
                        }
                        if(childNodes.val().dataEntrada2) {
                            p.horarioEntrada2 = ("0" + childNodes.val().dataEntrada2.hours).slice(-2) + ":" + ("0" + childNodes.val().dataEntrada2.minutes).slice(-2);
                        }
                        if(childNodes.val().dataSaida2) {
                            p.horarioSaida2 = ("0" + childNodes.val().dataSaida2.hours).slice(-2) + ":" + ("0" + childNodes.val().dataSaida2.minutes).slice(-2);
                        }
                        if(childNodes.val().dataEntrada3) {
                            p.horarioEntrada3 = ("0" + childNodes.val().dataEntrada3.hours).slice(-2) + ":" + ("0" + childNodes.val().dataEntrada3.minutes).slice(-2);
                        }
                        if(childNodes.val().dataSaida3) {
                            p.horarioSaida3 = ("0" + childNodes.val().dataSaida3.hours).slice(-2) + ":" + ("0" + childNodes.val().dataSaida3.minutes).slice(-2);
                        }
                        presencas.push(p);
                    })
                    JSONToCSVConvertor(presencas, "Relatório Presença", "Codigo;Entrada1;Saida1;Entrada2;Saida2;Entrada3;Saida3;");
                    return;
                })
            });

        }

        function getBlocos() {
            $scope.blocos = [];
            var blocos = firebase.database().ref('Bloco/');
            blocos.on('value', (snapshot) => {
                const data = snapshot.val();
                $timeout(function() {
                    for(var item in data) {
                        if(!($scope.blocos.includes(item)))
                            $scope.blocos.push(item);
                    }
                    $scope.blocoSelecionado = $scope.blocos[0];
                    getSalas($scope.blocos[0]);
                });
            });
        }

        function getSalas(bloco) {
            $scope.salas = [];
            var salas = firebase.database().ref('Bloco/' + bloco);
            salas.on('value', (snapshot) => {
                const data = snapshot.val();
                $timeout(function() {
                    for(var item in data){
                        if(item != "a") {
                            if(!($scope.salas.includes(item)))
                                $scope.salas.push(item);
                        }
                        
                    }
                    $scope.salaSelecionada = $scope.salas[0];
                });
            });
        }

        function getTurnoAtual(data) {
            if(data.getHours() >= 19)
                return 3;
            else if(data.getHours() >= 13)
                return 2;
            else
                return 1;
        }

        function getNumeroAula(data, turno) {
            var horas = data.getHours();
            var minutos = data.getMinutes();
            switch(turno) {
                case 1:
                    if(horas <= 8 && minutos <= 45)
                        return 1;
                    else
                        return 2;
                case 2:
                    if(horas <= 14 && minutos <= 45)
                        return 1;
                    else
                        return 2;
                case 3:
                    if(horas <= 20 && minutos <= 45)
                        return 1;
                    else
                        return 2;
                default:
                    return 0;
            }
        }

        function JSONToCSVConvertor(JSONData, ReportTitle, ShowLabel) {
            var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
        
            var CSV = '';
        
            CSV += ReportTitle + '\r\n\n';

            if (ShowLabel) {
                var row = "";

                arrData[200] = {Codigo: "", Entrada1: "", Saida1:"", Entrada2: "", Saida2:"", Entrada3: "", Saida3:""};

                for (var index in arrData[200]) {
                    row += index + ';';
                }

                row = row.slice(0, -1);
                CSV += row + '\r\n';
            }
        
            for (var i = 0; i < arrData.length; i++) {
                var row = "";
        
                for (var index in arrData[i]) {
                    row += '"' + arrData[i][index] + '";';
                }
        
                row.slice(0, row.length - 1);
                CSV += row + '\r\n';
            }
        
            if (CSV == '') {
                alert("Invalid data");
                return;
            }
        
            var fileName = "";
            fileName += ReportTitle.replace(/ /g,"_");
        
            var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);
    
            var link = document.createElement("a");
            link.href = uri;
        
            link.style = "visibility:hidden";
            link.download = fileName + ".csv";
        
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }

        initial();
    });
}).call(angular);
