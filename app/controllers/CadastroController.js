(function () {
    app.controller("CadastroController", function ($timeout, $scope, $location) {

        function initial() {
            
        }

        $scope.cadastrarProfessor = function(nome, sobrenome, codigo, senha, senhaRepetida) {
            $scope.cadastrando = true;
            var email = document.getElementById("login").value;

            if(nome == "" || nome == undefined || nome == null || sobrenome == "" || sobrenome == undefined || sobrenome == null 
            || codigo == "" || codigo == undefined || codigo == null || senha == "" || senha == undefined || senha == null 
            || senhaRepetida == "" || senhaRepetida == undefined || senhaRepetida == null ) {
                $scope.cadastrando = false;
                $scope.falhaCadastro = true;
                $scope.mensagem = "Preencha todos os campos!";
                setTimeout(() => {
                    $timeout(function() {
                        $scope.falhaCadastro = false;
                        $scope.mensagem = ""
                    });
                }, 3000)
                return;
            }

            if(senha != senhaRepetida) {
                $scope.cadastrando = false;
                $scope.falhaCadastro = true;
                $scope.mensagem = "As senhas precisam ser iguais!";
                setTimeout(() => {
                    $timeout(function() {
                        $scope.falhaCadastro = false;
                        $scope.mensagem = ""
                    });
                }, 3000)
                return;
            }

            firebase.auth().createUserWithEmailAndPassword(email, senha)
                .then((sucesso) => {
                    var idCadastro = sucesso.user.$.W;
                    const novoProfessor = {
                        nome: nome,
                        sobrenome: sobrenome,
                        codigo: codigo
                    }
                    firebase.database().ref('Professor/' + idCadastro).set(novoProfessor);
                    $timeout(function() {
                        $location.path("/inicio");
                    })
                }).catch((error) => {
                    $timeout(function() {
                        $scope.falhaCadastro = true;
                        $scope.cadastrando = false;
                        if(error.code == "auth/invalid-email") {
                            $scope.mensagem = "Email inválido.";
                            setTimeout(() => {
                                $timeout(function() {
                                    $scope.falhaCadastro = false;
                                    $scope.mensagem = ""
                                });
                            }, 3000)
                        } else if(error.code == "auth/weak-password") {
                            $scope.mensagem = "A senha precisa ter pelo menos 6 caracteres.";
                            setTimeout(() => {
                                $timeout(function() {
                                    $scope.falhaCadastro = false;
                                    $scope.mensagem = ""
                                });
                            }, 3000)
                        } else if(error.code == "auth/email-already-in-use") {
                            $scope.mensagem = "Email já cadastrado.";
                            setTimeout(() => {
                                $timeout(function() {
                                    $scope.falhaCadastro = false;
                                    $scope.mensagem = ""
                                });
                            }, 3000)
                        } else {
                            console.log("erro ao cadastrar.")
                        }
                    })
                })
        }

        $scope.voltar = function() {
            $location.path("/login")
        }

        initial();
    });
}).call(angular);
