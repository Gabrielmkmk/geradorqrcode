(function () {
    app.controller("SalaController", function ($timeout, $scope) {
        function initial() {
            getBlocos()
        }

        function getBlocos() {
            $scope.blocos = [];
            var blocos = firebase.database().ref('Bloco/');
            blocos.on('value', (snapshot) => {
                const data = snapshot.val();
                    $timeout(function() {
                        for(var item in data) {
                            if(!$scope.blocos.includes(item))
                                $scope.blocos.push(item);
                        }
                    });
            });
        }

        $scope.editarBloco = function(bloco) {
            $scope.edicaoBlocoSalaMenu = true;
            $scope.blocoParaEdicao = bloco;
            getSalas(bloco)
        }

        function getSalas(bloco) {
            $scope.salas = [];
            var salas = firebase.database().ref('Bloco/' + bloco);
            salas.on('value', (snapshot) => {
                const data = snapshot.val();
                $timeout(function() {
                    for(var item in data){
                        if(item != "a")
                            if(!$scope.salas.includes(item))
                                $scope.salas.push(item);
                    }
                });
            });
        }

        $scope.cancelarEdicao = function() {
            $scope.edicaoBlocoSalaMenu = false;
            $scope.blocoParaEdicao = undefined;
        }

        $scope.novaSala = function() {
            $scope.criandoNovaSala = true;
        }

        $scope.deletarSala = function(sala) {
            var deleteSala = firebase.database().ref('Bloco/' + $scope.blocoParaEdicao + "/" + sala);
            deleteSala.remove()
            .then(function() {
                getSalas($scope.blocoParaEdicao )
            })
        }

        $scope.voltar = function() {
            $scope.criandoNovaSala = false;
            document.getElementById("nomeSalaInput").value = "";
        }

        $scope.adicionarSala = function() {
            var numSala = document.getElementById("nomeSalaInput").value;
            if(numSala == undefined || numSala == null || numSala == "") {
                $scope.naoAdicionar = true;
                $scope.mensagemErroAdicionar = "Preencha a numeração da sala!";
    
                setTimeout(() => {
                    $timeout(function() {
                        $scope.naoAdicionar = false;
                        $scope.mensagemErroAdicionar = "";
                    });
                }, 3000)
                return;
            } else {
                firebase.database().ref('Bloco/' + $scope.blocoParaEdicao + "/" + numSala).set(
                    {2: {
                        1: {
                            1: {
                                nomeMateria: ""
                            },
                            2: {
                                nomeMateria: ""
                            }
                        }, 
                        2: {
                            1: {
                                nomeMateria: ""
                            },
                            2: {
                                nomeMateria: ""
                            }
                        },
                        3: {
                            1: {
                                nomeMateria: ""
                            },
                            2: {
                                nomeMateria: ""
                            }
                        },
                    },
                    3: {
                        1: {
                            1: {
                                nomeMateria: ""
                            },
                            2: {
                                nomeMateria: ""
                            }
                        }, 
                        2: {
                            1: {
                                nomeMateria: ""
                            },
                            2: {
                                nomeMateria: ""
                            }
                        },
                        3: {
                            1: {
                                nomeMateria: ""
                            },
                            2: {
                                nomeMateria: ""
                            }
                        },
                    },
                    4: {
                        1: {
                            1: {
                                nomeMateria: ""
                            },
                            2: {
                                nomeMateria: ""
                            }
                        }, 
                        2: {
                            1: {
                                nomeMateria: ""
                            },
                            2: {
                                nomeMateria: ""
                            }
                        },
                        3: {
                            1: {
                                nomeMateria: ""
                            },
                            2: {
                                nomeMateria: ""
                            }
                        },
                    },
                    5: {
                        1: {
                            1: {
                                nomeMateria: ""
                            },
                            2: {
                                nomeMateria: ""
                            }
                        }, 
                        2: {
                            1: {
                                nomeMateria: ""
                            },
                            2: {
                                nomeMateria: ""
                            }
                        },
                        3: {
                            1: {
                                nomeMateria: ""
                            },
                            2: {
                                nomeMateria: ""
                            }
                        },
                    },
                    6: {
                        1: {
                            1: {
                                nomeMateria: ""
                            },
                            2: {
                                nomeMateria: ""
                            }
                        }, 
                        2: {
                            1: {
                                nomeMateria: ""
                            },
                            2: {
                                nomeMateria: ""
                            }
                        },
                        3: {
                            1: {
                                nomeMateria: ""
                            },
                            2: {
                                nomeMateria: ""
                            }
                        },
                    },
                    7: {
                        1: {
                            1: {
                                nomeMateria: ""
                            },
                            2: {
                                nomeMateria: ""
                            }
                        }, 
                        2: {
                            1: {
                                nomeMateria: ""
                            },
                            2: {
                                nomeMateria: ""
                            }
                        },
                        3: {
                            1: {
                                nomeMateria: ""
                            },
                            2: {
                                nomeMateria: ""
                            }
                        },
                    }
                });
                $timeout(function() {
                    $scope.salaEditada = true;
                    $scope.mensagemSucesso = "Sala adicionada com sucesso!";
                    $scope.criandoNovaSala = false;
        
                    setTimeout(() => {
                        $timeout(function() {
                            $scope.salaEditada = false;
                            $scope.mensagemSucesso = "";
                        });
                    }, 3000)
                    var inputSala = document.getElementById("nomeSalaInput");
                    if(inputSala != null)
                        document.getElementById("nomeSalaInput").value = "";
                    getSalas($scope.blocoParaEdicao)
                    getBlocos();
                });
            }
        }

        initial();
    });
}).call(angular);
