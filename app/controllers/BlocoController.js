(function () {
    app.controller("BlocoController", function ($timeout, $scope) {

        $scope.edicaoBlock = false;

        function initial() {
            getBlocos()
        }

        function getBlocos() {
            $scope.blocos = [];
            var blocos = firebase.database().ref('Bloco/');
            blocos.on('value', (snapshot) => {
                const data = snapshot.val();
                    $timeout(function() {
                        for(var item in data) {
                            $scope.blocos.push(item);
                        }
                    });
            });
        }

        $scope.editarBloco = function(bloco) {
            $scope.preenchendoBloco = true;
            $scope.edicaoBlock = true;
            document.getElementById("nomeBlocoInput").value = bloco;
        }

        $scope.preencherBloco = function() {
            $scope.preenchendoBloco = true;
        }

        $scope.adicionarBloco = function() {
            var bloco = document.getElementById("nomeBlocoInput").value;
            if(bloco == undefined || bloco == null || bloco == "") {
                $scope.naoAdicionar = true;
                $scope.mensagemErroAdicionar = "Preencha o nome do bloco!";
    
                setTimeout(() => {
                    $timeout(function() {
                        $scope.naoAdicionar = false;
                        $scope.mensagemErroAdicionar = "";
                    });
                }, 3000)
                return;
            }

            firebase.database().ref('Bloco/' + bloco).set({a: ""});
            $timeout(function() {
                $scope.blocoEditado = true;
                $scope.mensagemSucesso = "Bloco adicionado com sucesso!";
    
                setTimeout(() => {
                    $timeout(function() {
                        $scope.blocoEditado = false;
                        $scope.mensagemSucesso = "";
                    });
                }, 3000)
                document.getElementById("nomeBlocoInput").value = "";
                getBlocos()
                $scope.preenchendoBloco = false;
            });
        }

        $scope.voltar = function() {
            $scope.preenchendoBloco = false;
            $scope.edicaoBlock = false;
            document.getElementById("nomeBlocoInput").value = "";
        }

        initial();
    });
}).call(angular);
